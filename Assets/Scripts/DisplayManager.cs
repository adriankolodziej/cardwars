﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayManager : MonoBehaviour
{
    private PhysicalCameraStreamer camStreamer;
    
    // Start is called before the first frame update
    void Start()
    {     
        camStreamer = GetComponentInChildren<PhysicalCameraStreamer>() as PhysicalCameraStreamer;
        if (!camStreamer)
            Debug.Log("PhysicalCameraStreamer component not found in children of Display!");
        
        //StartStreaming();
    }

    public void StartStreaming() => camStreamer.Play();
    public void Pause() => camStreamer.Pause();
    public void Stop() => camStreamer.Stop();
}