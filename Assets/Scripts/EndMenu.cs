﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EndMenu : MonoBehaviour
{
    private string menuScene = "Menu";
    private string scene = "SampleScene";

    public void LoadMenu()
    {
        
        SceneManager.LoadScene(menuScene);
    }

    public void Restart()
    {
        SceneManager.LoadScene(scene);

    }
    public void QuitGame()
    {
        Application.Quit();
    }
}