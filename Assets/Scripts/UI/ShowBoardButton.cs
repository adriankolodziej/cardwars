﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowBoardButton : MonoBehaviour
{
    [SerializeField]
    private bool boardVisible = false;

    private void Start()
    {
        GameEvents.current.onBoardVisibilityChanged += ChangeText;    
    }

    private void ChangeText()
    {
        boardVisible = !boardVisible;

        Text text = this.transform.GetComponentInChildren<Text>();
        if (text)
        {
            if (boardVisible == true)
            {
                text.text = "Hide\nBoard";
            }
            else
            {
                text.text = "Show\nBoard";
            }
        }
    }
}
