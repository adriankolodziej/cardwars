﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HandCardsPanel : MonoBehaviour
{
    public GameObject cardPrefab;
    [SerializeField]
    private Deck playerDeck;

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.onDeckInitialized += InitializeHand;
        GameEvents.current.onCardDrawn += AddDrawnCard;
    }

    public void InitializeHand()
    {
        playerDeck = PlayerClass.playerInstance.GetPlayerDeck();
        foreach (int i in playerDeck.Hand)
        {
            GameObject gameObject = playerDeck.GetCardPrefab(i);
            Card card = gameObject.GetComponent<Card>();
            card.InheritStatistics();
            AddCardToHand(card);
        }
        GameEvents.current.onDeckInitialized -= InitializeHand;
    }

    public void AddDrawnCard()
    {
        GameObject gameObject = playerDeck.GetCardPrefab(playerDeck.Hand.Last());
        Card card = gameObject.GetComponent<Card>();
        card.InheritStatistics();
        AddCardToHand(card);
    }

    public void AddCardToHand(Card card)
    {
        GameObject gameObject = Instantiate(cardPrefab);
        DraggableCard draggableCard = gameObject.GetComponent<DraggableCard>();
        draggableCard.transform.SetParent(this.transform);
        draggableCard.InitializeTexts(card);
    }
}
