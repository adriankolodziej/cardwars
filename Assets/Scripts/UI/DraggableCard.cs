﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DraggableCard : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Transform returnParent = null;       // parent object to return to after dropped
    public bool isDraggable = true;             // if the card can be dragged
    [SerializeField]
    private float scalingFactor = 0.6f;         // scale of dragged card
    [SerializeField]
    private DroppableArea droppableArea = null; // droppable area object
    public Card card;
    // Start is called before the first frame update
    void Start()
    {
        droppableArea = this.transform.parent.parent.GetComponentInChildren<DroppableArea>();
    }
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (isDraggable)
        {
            returnParent = this.transform.parent;
            this.transform.SetParent(this.transform.parent.parent);
            this.transform.localScale *= scalingFactor;  // scale the card down when dragged
            GetComponent<CanvasGroup>().blocksRaycasts = false;

            if (droppableArea && droppableArea.visible == false)
            {
                droppableArea.ChangeVisibility();   // show the target droppable object on canvas
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isDraggable)
        {
            this.transform.position = eventData.position;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (isDraggable)
        {
            LockOnDropped(false);
        }
    }

    public void LockOnDropped(bool locked)
    {
        this.transform.SetParent(returnParent);
        this.transform.localScale /= scalingFactor;  // restore original scale
        
        if(locked)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = false;
            //GetComponent<CanvasGroup>().alpha = 0f;
            isDraggable = false;
        }
        else
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            //GetComponent<CanvasGroup>().alpha = 1f;
        }
    }

    public void InitializeTexts(Card card)
    {
        this.card = card;
        card.SetCardRepresentation(this);
        Component[] texts;
        texts = this.transform.GetComponentsInChildren<Text>();
        foreach (Text text in texts)
        {
            if (text.name == "CardName")
            {
                text.text = card.name;
            }
            if (text.name == "HalthValueText")
            {
                text.text = card.GetHealth().ToString();
            }
            if (text.name == "DamageValueText")
            {
                text.text = card.GetDamage().ToString();
            }
            if (text.name == "FrequencyValueText")
            {
                text.text = card.GetSpawnRate().ToString() + "s";
            }
            if (text.name == "CountValueText")
            {
                text.text = card.GetNumberOfGeneratedUnits().ToString();
            }
            if (text.name == "PriceValueText")
            {
                text.text = card.GetPrice().ToString();
            }
        }
    }
}
