﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DroppableSlot : MonoBehaviour, IDropHandler
{
    private DroppableArea parentArea;
    [SerializeField]
    private Transform dropField;

    // Start is called before the first frame update
    void Start()
    {
        parentArea = this.transform.parent.GetComponent<DroppableArea>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (this.transform.childCount == 0)
        {
            DraggableCard draggable = eventData.pointerDrag.GetComponent<DraggableCard>();
            if (draggable)
            {
                if (PlayerClass.playerInstance.BuyCard(draggable.card.GetPrice()))
                {
                    draggable.returnParent = this.transform;
                    draggable.LockOnDropped(true);
                    PlayerClass.playerInstance.PlayCard(draggable.card.GetCardPrefabId());
                    Instantiate(draggable.card, dropField.position, dropField.rotation);
                }
             }     
        }
    }
}
