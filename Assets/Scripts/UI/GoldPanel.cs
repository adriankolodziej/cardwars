﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldPanel : MonoBehaviour
{
    [SerializeField]
    private int goldValue;
    [SerializeField]
    private Text goldValueText;

    // Start is called before the first frame update
    void Start()
    {
        goldValueText = this.transform.GetComponentInChildren<Text>();
        SetGoldValue();
        GameEvents.current.onGoldChanged += SetGoldValue;
    }

    private void SetGoldValue()
    {
        goldValue = PlayerClass.playerInstance.GetPlayerGold();
        goldValueText.text = goldValue.ToString();
    }
}
