﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppableArea : MonoBehaviour
{
    public bool visible = false;

    private void Show()
    {
        // Change transparency and blocking of raycasts
        this.transform.GetComponent<CanvasGroup>().alpha = 1f;
        this.transform.GetComponent<CanvasGroup>().blocksRaycasts = true;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            Transform slot = this.transform.GetChild(i);
            if (slot.childCount > 0)
            {
                DraggableCard card = slot.GetComponentInChildren<DraggableCard>();
                card.GetComponent<CanvasGroup>().alpha = 1f;
            }
        }

    }
    private void Hide()
    {
        // Change transparency and blocking of raycasts
        this.transform.GetComponent<CanvasGroup>().alpha = 0f;
        this.transform.GetComponent<CanvasGroup>().blocksRaycasts = false;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            Transform slot = this.transform.GetChild(i);
            if (slot.childCount > 0)
            {
                DraggableCard card = slot.GetComponentInChildren<DraggableCard>();
                card.GetComponent<CanvasGroup>().alpha = 0f;
            }
        }
    }

    public void ChangeVisibility()
    {
        visible = !visible;
        // trigger event when visibility changed
        GameEvents.current.BoardVisibilityChanged();

        if(visible == true)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }
}
