﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckPanel : MonoBehaviour
{
    [SerializeField]
    private int deckCount;
    [SerializeField]
    private Text deckCountText;

    // Start is called before the first frame update
    void Start()
    {
        deckCountText = this.transform.GetComponentInChildren<Text>();
        GameEvents.current.onCardDrawn += SetDeckCount;
        GameEvents.current.onDeckInitialized += SetDeckCount;
    }

    private void SetDeckCount()
    {
        deckCount = PlayerClass.playerInstance.GetPlayerDeck().ListOfCards.Count;
        Debug.Log(deckCount);
        deckCountText.text = deckCount.ToString();
    }
}
