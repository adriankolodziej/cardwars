﻿using Assets.Scripts.Units;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
	//Statistics that are shown on the card GameObject
	[SerializeField]
	private int Health;
	[SerializeField]
	private int Damage;
	[SerializeField]
	private float SpawnRate;
	[SerializeField]
	private int Tier;
	[SerializeField]
	private int NumberOfGeneratedUnits;
	[SerializeField]
	private int Price;
	public bool Enemy = false;
	[SerializeField]
	private Unit unit; //Shows which unit will be spawn when this card is played
	[SerializeField]
	private int CardPrefabId;
	[SerializeField]
	private DraggableCard CardRepresentation;

	private int spawnCounter = 0; //counter counts how many units were summoned

	void Start()
	{
		InheritStatistics();
		//calling function Spawn with time between spawning equaling to SpawnRate and with 1 second delay - this should be moved to place, where card is put on the table
		InvokeRepeating("Spawn", 1f, SpawnRate); 
	}

	void Update()
	{
	}

    private void OnDrawGizmos()
	{
		Vector3 sphere = this.transform.position;
		if (Enemy)
		{
			sphere.z = this.transform.position.z - 2f;
		}
		else
		{
			sphere.z = this.transform.position.z + 2f;
		}
		Gizmos.DrawSphere(sphere, 0.5f);
    }

    /**
	 * Card inherits statistics from units they spawns
	 */
    public void InheritStatistics()
	{
		Health = unit.GetHealth();
		Damage = unit.GetDamage();
		SpawnRate = unit.GetSpawnRate();
		NumberOfGeneratedUnits = unit.GetNumberOfGeneratedUnits();
		Tier = unit.GetTier();
        PlayerClass.playerInstance.OnCalculateCardPrice(this);
    }

	public void SetPrice(int price)
	{
		Price = price;
	}

	public void SetCardPrefabId(int cardPrefabId)
    {
		CardPrefabId = cardPrefabId;
    }

	public void SetCardRepresentation(DraggableCard cardRepresentation)
    {
		CardRepresentation = cardRepresentation;
    }

	//This method spawns physical troops into the game - if there is space
	void Spawn()
	{
		Vector3 sphere = this.transform.position;
		if (Enemy) {
			sphere.z = this.transform.position.z - 2f;
		} else
		{
			sphere.z = this.transform.position.z + 2f;
		}
		Collider[] intersecting = Physics.OverlapSphere(sphere, 0.5f);
		//spawning units until number of spawned equals NumberOfGeneratedUnits
		if (NumberOfGeneratedUnits > spawnCounter && intersecting.Length <= 1)
		{
			unit.Spawn(transform, Enemy);
			spawnCounter += 1;
		}
		else if (Enemy && intersecting.Length <= 1)
		{
			//remove card from board if all units are spawned. Dispatching new event to enemy class, that he can place another card
			if (this.transform.position.x == 32.5f)
			{
				GameEvents.current.EnemyCardDispawnedMiddle();
			}
			else if (this.transform.position.x == 30.5f)
			{
				GameEvents.current.EnemyCardDispawnedTop();
			}
			else if (this.transform.position.x == 34.5f)
			{
				GameEvents.current.EnemyCardDispawnedBottom();
			}
		}
		if(spawnCounter >= NumberOfGeneratedUnits)
        {
			if (CardRepresentation != null && CardRepresentation.gameObject != null)
			{
				Destroy(CardRepresentation.gameObject);
			}
			if (gameObject != null)
			{
				Destroy(gameObject);
			}
        }
	}

	public int GetHealth() { return Health; }
	public int GetDamage() { return Damage; }
	public float GetSpawnRate() { return SpawnRate; }
	public int GetTier() { return Tier; }
    public int GetPrice() { return Price; }
	public int GetNumberOfGeneratedUnits() { return NumberOfGeneratedUnits; }
	public int GetCardPrefabId() { return CardPrefabId; }
}
