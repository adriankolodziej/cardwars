﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClass : MonoBehaviour
{
    //enemys deck
    public Deck deck = new Deck();
    //counter to control amount of cards in deck
    public int cardsCounter = 0;
    //list of enemys cards - used mostly to keep track of how much cards were played
    List<int> cards;
    //Number of cards on the board shouldn't be more than 3
    int cardsOnBoardCounter = 0;

    List<GameObject> cardsOnBoard = new List<GameObject>();

    List<float> positionsAvailable = new List<float>();

    // Start is called before the first frame update
    void Start()
    {
        positionsAvailable.Add(12.5f);
        positionsAvailable.Add(10.5f);
        positionsAvailable.Add(8.5f);
        //initialization of enemys deck
        deck.GenerateDeck();
        deck.Enemy = true;
        cards = deck.ListOfCards;
        for (int i = 0; i < 5; i++) {
            deck.AddCardToHand();
        }
        //placing cards every 10 seconds - at random for now
        InvokeRepeating("PlaceCard", 2f, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        GameEvents.current.onEnemyCardDispawnTop += CardDispawnedTop;
        GameEvents.current.onEnemyCardDispawnMiddle += CardDispawnedMiddle;
        GameEvents.current.onEnemyCardDispawnBottom += CardDispawnedBottom;
    }

    /**
     * Next 3 functions just remaps x coordinate
     */
    void CardDispawnedTop()
    {
        float x = 8.5f;
        CardDispawned(x);
    }

    void CardDispawnedMiddle()
    {
        float x = 12.5f;
        CardDispawned(x);
    }

    void CardDispawnedBottom()
    {
        float x = 14.5f;
        CardDispawned(x);
    }

    /**
     * If card has despawned - it's safe to place another one on the board
     */
    void CardDispawned(float x)
    {
        if (cardsOnBoard.Count > 0)
        {
            cardsOnBoardCounter -= 1;
            for (int i = 0; i < cardsOnBoard.Count; i++)
            {
                if (cardsOnBoard[i].transform.position.x == x)
                {
                    //deleting used cards, and expand list with available positions for cards
                    Destroy(cardsOnBoard[i]);
                    cardsOnBoard.RemoveAt(i);
                    positionsAvailable.Add(x);
                }
            }
        }
    }

    /**
     * Function takes a card (at that point just numeric value of a card), and tries to instantiate an object
     * Than function removes card from hand, and draws another one
     */
    void PlaceCard() {
        if (cardsCounter < cards.Count && cardsOnBoardCounter < 3 && positionsAvailable.Count > 0) {
            //get actual card from deck and instantiate it
            GameObject card = deck.GetCardPrefab(deck.Hand[0]);
            GameObject a = Instantiate(card);
            //one of three random positions on scene
            //getting position of object that's spawning unit
            Vector3 position = GetPosition();

            //removing position on board from available spaces to place card
            positionsAvailable.Remove(position.x);

            //moving spawned card
            a.transform.position = position;
            cardsOnBoardCounter += 1;

            cardsOnBoard.Add(a);
            replaceCardInHand();
        }
    }

    /**
     * Function returns position on board for spawning card
     */
    Vector3 GetPosition() {
        //Randomizing next random spot for card from available spaces on board
        System.Random rnd = new System.Random();
        int posId = rnd.Next(positionsAvailable.Count);
        float xCoord = positionsAvailable[posId];

        Vector3 result = new Vector3(xCoord, 0.5f, 10.5f);

        return result;
    }

    /**
     * Function removes used card from hand and replaces it with another
     */
    void replaceCardInHand()
    {
        deck.Hand.RemoveAt(0);
        deck.AddCardToHand();
    }
}
