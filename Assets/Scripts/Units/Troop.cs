﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Units
{
	public class Troop : MonoBehaviour
	{

		public float blink;
		public float buffer;

		private float blinkTime = 0;
		private float bufferTime = 0;

		public Renderer troop;

		//Troop statistics
		[SerializeField]
		public int Health;
		[SerializeField]
		protected int Damage;
		[SerializeField]
		protected float Range;
		[SerializeField]
		protected float Speed;
		[SerializeField]
		protected int AttackDelay;
		[SerializeField]
		protected int Tier;

		protected int MaxHealth = 40;

		protected float lastAttack;

		protected bool isAttacking;
		protected bool isWaiting;

		protected String Owner; //An owner of the unit, used for determining whether this unit has to attack detected unit or not
		protected String Fortress; 

		protected Transform attackedFoe; //A unit that should be attacked
		void Start()
		{

		}

		void Update()
		{
			
		}

		public void InheritStatistics(Unit unit) //This method is used to get unit statistics and give them to a physical troop
		{
			Health = unit.GetHealth();
			MaxHealth = unit.GetHealth();
			Damage = unit.GetDamage();
			Range = unit.GetRange();
			Speed = unit.GetSpeed();
			AttackDelay = unit.GetAttackDelay();
			Tier = unit.GetTier();
		}

		public void Move() //Moves transform forward according to Speed field
		{
			transform.Translate(0, 0, Speed * Time.deltaTime);
		}

		public void Attack() //If enough time has passed this troop will deal damage to the enemy troop
		{
			if (lastAttack >= AttackDelay && attackedFoe != null)
			{
				lastAttack = 0;
				attackedFoe?.SendMessage("RecieveDamage",Damage);
			}
			lastAttack += Time.deltaTime;
		}

		/*
		 * This method casts a raycast to find obstacles ahead of this troop
		 * It is virtual beacuse some troops may use different detection system
		 * If the raycast finds a friendly troop, this troop will only follow the one ahead
		 * On the other hand if it detects enemy troop it will go into attack mode
		 */
		public virtual void DetectEnemy()
		{
			RaycastHit raycastHit;
			Ray ray = new Ray(transform.position, transform.forward);
			Debug.DrawRay(transform.position, transform.forward);
			if (Physics.Raycast(ray, out raycastHit, Range))
			{
				if (raycastHit.transform.tag != Owner && raycastHit.transform.tag != this.Fortress)
				{
					isAttacking = true;
					isWaiting = false;
					attackedFoe = raycastHit.transform;
				}
				else if (raycastHit.transform.tag == Owner)
				{
					if (raycastHit.distance < 1)
					{
						isAttacking = false;
						isWaiting = true;
					}
					else
						isWaiting = false;
				}
			}
			else if (Physics.Raycast(ray, out raycastHit, 5f))
			{		
				if(raycastHit.transform.tag == Fortress)
				{
					isAttacking = true;
					attackedFoe = raycastHit.transform;
				}
			}
			else
			{
				isAttacking = false;
				isWaiting = false;
			}
		}

		public void RecieveDamage(int damage) //Receieving damage and death if Health goes to zero or below
		{
			bufferTime = buffer;
			blinkTime = blink;

			Health -= damage;
			if (Health <= 0)
			{
				if (Owner.ToLower() == "foe")
				{
					PlayerClass.playerInstance.OnEnemyUnitDied(this);
				}
				Die();
			}

			
		}
		public void Die()
		{
			Destroy(gameObject);
		}

		public void Blink()
		{
			if (bufferTime > 0)
			{
				bufferTime -= Time.deltaTime;
				blinkTime -= Time.deltaTime;

				if (blinkTime <= 0)
				{
					troop.enabled = !troop.enabled;
					blinkTime = blink;
				}


				if (bufferTime <= 0)
				{
					troop.enabled = true;
				}
			}
		}
		public int GetMaxHealth() { return MaxHealth; }
		public int GetDamage() { return Damage; }
		public int GetTier() { return Tier; }
	}
}
