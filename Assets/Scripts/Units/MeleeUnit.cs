﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Units
{
	[CreateAssetMenu(fileName = "New Unit", menuName = "Units/MeleeUnit")]
	class MeleeUnit: Unit
	{		
		//This is MeleeUnit, used for clarity
	}
}
