﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Units
{
	public class Archer : Troop
	{
		[SerializeField]
		private LayerMask IgnoredLayer;
		void Start()
		{
			Owner = transform.tag;
			if (Owner == "Friend")
				this.Fortress = "FriendFortress";
			else this.Fortress = "FoeFortress";
			isAttacking = false;
			isWaiting = false;
		}

		void Update()
		{
			DetectEnemy();
			if (isAttacking)
			{
				Attack();
			}
			else if (!isWaiting)
			{
				Move();
			}

			Blink();
		}

		/*
		 * Archer uses override because it has the ability to attack from distance
		 * Ignoring collision was needed and that's why Archer casts 2 raycast
		 * First with a LayerMask that should be ignored -> this one is used for detecting foes
		 * Every Troop should have LayerMask parameter set to suitable value
		 * Second similar to the one in base class -> used for detecting friends
		 */
		public override void DetectEnemy()
		{
			RaycastHit raycastHit;
			Ray ray = new Ray(transform.position, transform.forward);
			Debug.DrawRay(transform.position, transform.forward);
			if (Physics.Raycast(ray, out raycastHit, Range, ~IgnoredLayer))
			{
				if (raycastHit.transform.tag != Owner)
				{
					isAttacking = true;
					isWaiting = false;
					attackedFoe = raycastHit.transform;
				}
			}
			else if (Physics.Raycast(ray, out raycastHit, 5f))
			{
				if (raycastHit.transform.tag != Fortress)
				{
					isAttacking = true;
					attackedFoe = raycastHit.transform;
					Debug.Log("Encountered " + this.Fortress);
				}
			}
			else
			{
				isAttacking = false;
				isWaiting = false;
			}

			if (Physics.Raycast(ray, out raycastHit, 1))
			{
				if (raycastHit.transform.tag == Owner)
				{
					isAttacking = false;
					isWaiting = true;
				}
				else
				{
					isWaiting = false;
				}
			}

		}
	}
}
