﻿using Assets.Scripts.Units;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fortress : MonoBehaviour
{
    public float Health;
    public float MaxHealth = 100;
    public float blink;
    public float buffer;

    private float blinkTime = 0;
    private float bufferTime = 0;

    public Renderer fortress;
    public GameObject HealthBarObject;
    public Slider HealthBar;

    void Start()
    {
        Health = MaxHealth;
        HealthBar.value = UpdateHelath();
    }

    void Update()
    {
        HealthBar.value = UpdateHelath();

            if (bufferTime > 0)
            {
                bufferTime -= Time.deltaTime;
                blinkTime -= Time.deltaTime;

                if (blinkTime <= 0)
                {
                    fortress.enabled = !fortress.enabled;
                    blinkTime = blink;
                }

                
                if (bufferTime <= 0)
                {
                    fortress.enabled = true;
                }
            }

        if(Health < MaxHealth)
        {
            HealthBarObject.SetActive(true);
        }

        if(Health <= 0)
        {
            Die();
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    void RecieveDamage(int damage)
	{
            Health -= (float)damage;
            bufferTime = buffer;
            blinkTime = blink;    
	}
    public void Die()
    {
        Destroy(gameObject);
    }

    float UpdateHelath()
    {
        return Health/MaxHealth;
    }
}
