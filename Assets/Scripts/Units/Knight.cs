﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Units
{
	public class Knight : Troop
	{
		void Start()
		{
			Owner = transform.tag;
			if (Owner == "Friend")
				this.Fortress = "FoeFortress";
			else this.Fortress = "FriendFortress";
			isAttacking = false;
			isWaiting = false;
		}

		void Update()
		{
			DetectEnemy();
			if (isAttacking)
			{
				Attack();
			}
			else if(!isWaiting)
			{
				Move();
			}

			Blink();
		}

		public override void DetectEnemy()
		{
			base.DetectEnemy();
		}
	}
}
