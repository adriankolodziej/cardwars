﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Units
{
	[CreateAssetMenu(fileName = "New Unit", menuName = "Units")]
	public class Unit : ScriptableObject
	{
		//Statistics to be shown on the card item in-game and to be read by a unit object
		[SerializeField]
		private int Health;
		[SerializeField]
		private int Damage;
		[SerializeField]
		private float Range;
		[SerializeField]
		private float Speed;
		[SerializeField]
		private int AttackDelay;
		[SerializeField]
		private int Tier;
		[SerializeField]
		private float SpawnRate;
		[SerializeField]
		private int NumberOfGeneratedUnits;
		[SerializeField]
		public GameObject unitPrefab; //Prefab of a spawned unit

		/*
		 * Spawner creates new object using instantiate on prefabs and puts them into place on scene by transform.position
		 */
		public void Spawn(Transform transform, Boolean enemy)
		{
			GameObject a = Instantiate(unitPrefab);
			//getting position of object that's spawning unit
			Vector3 position = transform.position;

			//calculating new position - forward to the spawn point
			if (enemy)
			{
				a.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.Lerp(a.GetComponent<MeshRenderer>().material.color, Color.blue, 0.5f));
				a.transform.Rotate(new Vector3(0, 180, 0));
				a.transform.tag = "Foe";
				a.layer = LayerMask.NameToLayer("Foe");
				position.z -= 2f;
			}
			else
			{
				a.transform.tag = "Friend";
				a.layer = LayerMask.NameToLayer("Friend");
				position.z += 2f;
			}
			//moving spawned unit
			a.transform.position = position;
			a.SendMessage("InheritStatistics", this);
		}

		//Bunch of getters for statistics
		public int GetHealth() { return Health; }
		public int GetDamage() { return Damage; }
		public float GetRange() { return Range; }
		public float GetSpeed() { return Speed; }
		public int GetAttackDelay() { return AttackDelay; }
		public float GetSpawnRate() { return SpawnRate; }
		public int GetTier() { return Tier; }
		public int GetNumberOfGeneratedUnits() { return NumberOfGeneratedUnits; }
	}
}
