﻿using Assets.Scripts.Units;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerClass : MonoBehaviour
{
    // Player game attributes
    [SerializeField]
    private int playerGold = 0;
    [SerializeField]
    private Deck deck;
    // Player economy class instance
    private PlayerEconomy economy;

    // Singleton instance
    #region Singleton
    public static PlayerClass playerInstance;

    private void Awake()
    {
        if (playerInstance == null)
            playerInstance = this;
        else
            Destroy(this);
    }
    #endregion

    // Spawning player economy
    private void Start()
    {
        economy = new PlayerEconomy();
        deck = new Deck();
        deck.GenerateDeck();
        for (int i = 0; i < 3; i++)
        {
            deck.AddCardToHand();
        }
        GameEvents.current.DeckInitialized();
    }

    // Custom events implementing various game functionality
    public event Action<Card> CalculateCardPrice;
    public event Action<Troop> EnemyUnitDied;

    // CardBuyAttempt event invoker
    public void OnCalculateCardPrice(Card playedCard)
    {
        CalculateCardPrice?.Invoke(playedCard);
    }

    // EnemyUnitDied event invoker
    public void OnEnemyUnitDied(Troop troop)
    {
        EnemyUnitDied?.Invoke(troop);
    }

    // Function used to check maths behind card buy
    public bool BuyCard(int cardPrice)
    {
        if (playerGold >= cardPrice)
        {
            SetGold(cardPrice * -1);
            return true;
        }
        else
            return false;
    }

    // Function used to draw a new card from deck
    public void DrawCard()
    {
        if (playerGold >= 5 && deck.Hand.Count < 5)
        {
            deck.AddCardToHand();          
            GameEvents.current.CardDrawn();
            SetGold(-5);
        }
    }

    public void PlayCard(int cardId)
    {
        if(deck != null)
        {
            deck.RemoveCardFromHand(cardId);
        }
    }

    // Function used to change player's gold variable and invoke UI change
    public void SetGold(int amountToChange)
    {
        playerGold += amountToChange;
        GameEvents.current.GoldChanged();
    }

    public int GetPlayerGold() { return playerGold; }
    public Deck GetPlayerDeck() { return deck;  }
}
