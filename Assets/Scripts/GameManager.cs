﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool isFinished = false;
    public GameObject EndGameUI;
    public Fortress player;
    public GameObject textWin;
    public GameObject textLose;



    public void EndGame()
    {
        EndGameUI.SetActive(false);

        if (isFinished == false)
        {
            isFinished = true;
            Debug.Log("Game Over");
            Time.timeScale = 0f;
            EndGameUI.SetActive(true);

            if (player.Health <= 0)
            {
                textLose.SetActive(true);
            }
            else
            {
                textWin.SetActive(true);
            }
        }
    }

}
