﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;
    public event Action onGoldChanged;
    public event Action onBoardVisibilityChanged;
    public event Action onCardDrawn;
    public event Action onDeckInitialized;
    public event Action onEnemyCardDispawnTop;
    public event Action onEnemyCardDispawnMiddle;
    public event Action onEnemyCardDispawnBottom;

    private void Awake()
    {
        current = this;
    }

    public void GoldChanged()
    {
        if(onGoldChanged != null)
        {
            onGoldChanged();
        }
    }

    public void EnemyCardDispawnedTop()
    {
        if (onEnemyCardDispawnTop != null)
        {
            onEnemyCardDispawnTop();
        }
    }

    public void EnemyCardDispawnedMiddle()
    {
        if (onEnemyCardDispawnMiddle != null)
        {
            onEnemyCardDispawnMiddle();
        }
    }

    public void EnemyCardDispawnedBottom()
    {
        if (onEnemyCardDispawnBottom != null)
        {
            onEnemyCardDispawnBottom();
        }
    }

    public void BoardVisibilityChanged()
    {
        if(onBoardVisibilityChanged != null)
        {
            onBoardVisibilityChanged();
        }
    }

    public void CardDrawn()
    {
        if(onCardDrawn != null)
        {
            onCardDrawn();
        }
    }

    public void DeckInitialized()
    {
        if (onDeckInitialized != null)
        {
            onDeckInitialized();
        }
    }
}
