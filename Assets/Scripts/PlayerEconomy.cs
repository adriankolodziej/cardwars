﻿using Assets.Scripts.Units;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEconomy
{
    // Assigning functions to event dispatchers on default constructor
    public PlayerEconomy()
    {
        PlayerClass.playerInstance.CalculateCardPrice += calculatePrice;
        PlayerClass.playerInstance.EnemyUnitDied += calculateGoldReward;        
    }

    // Function that calcualtes gold price for the card
    private void calculatePrice(Card playedCard)
    {
        int cardPrice = 0;
        cardPrice = playedCard.GetTier() + (playedCard.GetDamage()) + (playedCard.GetHealth() / 16);
        playedCard.SetPrice(cardPrice);
    }

    // Function that calculates amount of moeny that player gets after killing enemy unit
    private void calculateGoldReward(Troop troop)
    {
        int goldReward = 0;
        goldReward = troop.GetTier() + (troop.GetDamage() / 2) + (troop.GetMaxHealth() / 12);

        PlayerClass.playerInstance.SetGold(goldReward);
    }
}
